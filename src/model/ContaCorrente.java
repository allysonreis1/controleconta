package model;

public class ContaCorrente extends Conta{

	private Double tarifaManutencao;
	private String talaoCheque;
	private String cartaoCretido;
	private String chequeEspecial;
	
	public ContaCorrente (String numeroConta) {
		super(numeroConta);
	}
	
	public Double getTarifaManutencao() {
		return tarifaManutencao;
	}
	public void setTarifaManutencao(Double tarifaManutencao) {
		this.tarifaManutencao = tarifaManutencao;
	}
	public String getTalaoCheque() {
		return talaoCheque;
	}
	public void setTalaoCheque(String talaoCheque) {
		this.talaoCheque = talaoCheque;
	}
	public String getCartaoCretido() {
		return cartaoCretido;
	}
	public void setCartaoCretido(String cartaoCretido) {
		this.cartaoCretido = cartaoCretido;
	}
	public String getChequeEspecial() {
		return chequeEspecial;
	}
	public void setChequeEspecial(String chequeEspecial) {
		this.chequeEspecial = chequeEspecial;
	}
	
	
}