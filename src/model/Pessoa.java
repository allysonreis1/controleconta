package model;

import java.util.ArrayList;

public class Pessoa {
	private String nome;
    private String rg;
    private ArrayList<String> telefone;
    private String tipo;
    
    public Pessoa (String nome) {
    	this.nome = nome;
    }
    
    public String getTipo () {
        return tipo;
    }
    
    public void setTipo (String tipo) {
        this.tipo = tipo;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<String> getTelefone() {
        return telefone;
    }

    public void setTelefone(ArrayList<String> telefone) {
        this.telefone = telefone;
    }
    
    public String getRg () {
        return rg;
    }
    
    public void setRg (String rg) {
        this.rg = rg;
    }
}
