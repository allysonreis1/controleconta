package controller;

import java.util.ArrayList;

import model.Conta;
import model.ContaCorrente;
import model.ContaPoupanca;

public class ControleConta {
	private ArrayList<Conta> listaConta;

	public ControleConta () {
		this.listaConta = new ArrayList<Conta>();
	}
	
	public ArrayList<Conta> getListaConta() {
		return listaConta;
	}

	public void setListaConta(ArrayList<Conta> listaConta) {
		this.listaConta = listaConta;
	}
	
	public void adicionaConta (ContaPoupanca umaConta) {
		this.listaConta.add(umaConta);
	}
	
	public void adicionaConta (ContaCorrente umaConta) {
		this.listaConta.add(umaConta);
	}
	
	public void removerConta (ContaCorrente umaConta) {
		this.listaConta.remove(umaConta);
	}
	
	public void removerConta (ContaPoupanca umaConta) {
		this.listaConta.remove(umaConta);
	}
}
