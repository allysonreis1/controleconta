package controller;

import java.util.ArrayList;

import model.Pessoa;
import model.PessoaFisica;
import model.PessoaJuridica;

public class ControlePessoa {
	private ArrayList<Pessoa> listaPessoas;
    
    public ControlePessoa () {
        listaPessoas = new ArrayList<Pessoa>();
    }
    
    public ArrayList<Pessoa> getListaPessoaes() {
        return listaPessoas;
    }

    public void setListaPessoaes(ArrayList<Pessoa> listaPessoas) {
        this.listaPessoas = listaPessoas;
    }
    
    public String adicionar (PessoaFisica Pessoa) {
        listaPessoas.add(Pessoa);
        return "Pessoa Pessoa Física adicionado com sucesso.";
    }
    
    public String adicionar (PessoaJuridica Pessoa) {
        listaPessoas.add(Pessoa);
        return "Pessoa Pessoa Jurídica adicionado com sucesso.";
    }
    
    public void remover (PessoaJuridica Pessoa) {
        listaPessoas.remove(Pessoa);
    }
    
    public void remover (PessoaFisica Pessoa) {
        listaPessoas.remove(Pessoa);
    }
    
    public Pessoa pesquisarPessoa (String umNome) {
        for (Pessoa umPessoa : listaPessoas) {
            if (umPessoa.getNome().equalsIgnoreCase(umNome)) {
                return umPessoa;
            }
        }
        return null;
    }
}
